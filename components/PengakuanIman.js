import { Box, Flex, Text } from "@chakra-ui/react"

export default function PengakuanIman() {
  return (
    <Flex
      id="PengakuanIman"
      justify={"center"}
      direction="column"
      align="center"
      className="praying-bg"
    >
      <Text fontSize={24} as="b" mt={10} color="white">
        Pengakuan Iman GSPDI
      </Text>
      <Box maxW={1080} p={10} color="white">
        <Text as="p">
          “Pengakuan” Berdasarkan Roma 10:9-10 “Sebab jika kamu mengaku dengan
          mulutmu, bahwa Yesus adalah Tuhan, dan percaya dalam hatimu, bahwa
          Allah telah membangkitkan Dia dari antara orang mati, maka kamu akan
          diselamatkan. Karena dengan hati orang percaya dan dibenarkan, dan
          dengan mulut orang mengaku dan diselamatkan.” ;
        </Text>
        <br />
        <Text as="p">
          Matius 10:32, “Setiap orang yang mengakui Aku di depan manusia, Aku
          juga akan mengakuinya di depan Bapa-Ku yang di sorga.” Dan pengakuan
          Petrus dalam Matius 16:15-16. Pengakuan Iman adalah pernyataan sikap
          iman yang ditunjukkan melalui perkataan, diakui di hadapan umum oleh
          seseorang ataupun bersama-sama dalam persekutuan orang-orang yang
          sama-sama mengaku.
        </Text>
        <br />
        <Text as="p">
          Seperti terlihat pada saat Yosua menyatakan imannya dan diikuti oleh
          seluruh bangsa Israel (Yosua 24:15-24). Inilah yang menjadi dasar
          dibuatnya Pengakuan Iman oleh Gereja Sidang Pantekosta Di Indonesia.
        </Text>
        <br />
        <Text as="p">
        Pengakuan Iman juga adalah ringkasan dari semua pokok kepercayaan Kekristenan dan dasar dari semua doktrin yang dipegang oleh sebuah sinode gereja. Maka dari itu, sebuah pengakuan iman haruslah mencerminkan pandangan umum sinode mengenai doktrin dasar kekristenan.
        </Text>
        <br />
        <Text as="p">
        Dari Pengakuan Imannya, Sinode dapat mengembangkannya menjadi sebuah pandangan yang lebih luas mengenai pemahamannya terhadap doktrin-doktrin Alkitab tersebut, menetapkannya menjadi batasan-batasan berbagai penafsiran dan mengajarkannya di Sekolah Alkitab atau Sekolah Tinggi Teologi milik Sinode.
        </Text>
        <br />
        <Text as="p">
        Mengingat pentingnya Pengakuan Iman dalam penjabaran doktrin, maka, dengan rendah hati kami mencoba untuk merancang sebuah Pengakuan Iman yang baru yang dirasa lebih mencerminkan doktrin-doktrin tersebut dibanding Pengakuan Iman yang lama.
        </Text>
      </Box>
    </Flex>
  )
}
